<?php

namespace app\controllers;

use Yii;
use app\models\DirigeSeries;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DirigeSeriesController implements the CRUD actions for DirigeSeries model.
 */
class DirigeSeriesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DirigeSeries models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DirigeSeries::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DirigeSeries model.
     * @param string $idserie
     * @param string $iddirector
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idserie, $iddirector)
    {
        return $this->render('view', [
            'model' => $this->findModel($idserie, $iddirector),
        ]);
    }

    /**
     * Creates a new DirigeSeries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DirigeSeries();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idserie' => $model->idserie, 'iddirector' => $model->iddirector]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DirigeSeries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idserie
     * @param string $iddirector
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idserie, $iddirector)
    {
        $model = $this->findModel($idserie, $iddirector);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idserie' => $model->idserie, 'iddirector' => $model->iddirector]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DirigeSeries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idserie
     * @param string $iddirector
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idserie, $iddirector)
    {
        $this->findModel($idserie, $iddirector)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DirigeSeries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idserie
     * @param string $iddirector
     * @return DirigeSeries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idserie, $iddirector)
    {
        if (($model = DirigeSeries::findOne(['idserie' => $idserie, 'iddirector' => $iddirector])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
