<?php

namespace app\controllers;

use Yii;
use app\models\Es;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EsController implements the CRUD actions for Es model.
 */
class EsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Es models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Es::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Es model.
     * @param string $idgenero
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idgenero, $idpelicula)
    {
        return $this->render('view', [
            'model' => $this->findModel($idgenero, $idpelicula),
        ]);
    }

    /**
     * Creates a new Es model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Es();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idgenero' => $model->idgenero, 'idpelicula' => $model->idpelicula]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Es model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idgenero
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idgenero, $idpelicula)
    {
        $model = $this->findModel($idgenero, $idpelicula);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idgenero' => $model->idgenero, 'idpelicula' => $model->idpelicula]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Es model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idgenero
     * @param string $idpelicula
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idgenero, $idpelicula)
    {
        $this->findModel($idgenero, $idpelicula)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Es model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idgenero
     * @param string $idpelicula
     * @return Es the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idgenero, $idpelicula)
    {
        if (($model = Es::findOne(['idgenero' => $idgenero, 'idpelicula' => $idpelicula])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
