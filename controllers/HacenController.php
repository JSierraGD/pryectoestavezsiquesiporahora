<?php

namespace app\controllers;

use Yii;
use app\models\Hacen;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HacenController implements the CRUD actions for Hacen model.
 */
class HacenController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Hacen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Hacen::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hacen model.
     * @param string $idusuario
     * @param integer $idreview
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idusuario, $idreview)
    {
        return $this->render('view', [
            'model' => $this->findModel($idusuario, $idreview),
        ]);
    }

    /**
     * Creates a new Hacen model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Hacen();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idusuario' => $model->idusuario, 'idreview' => $model->idreview]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Hacen model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idusuario
     * @param integer $idreview
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idusuario, $idreview)
    {
        $model = $this->findModel($idusuario, $idreview);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idusuario' => $model->idusuario, 'idreview' => $model->idreview]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Hacen model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idusuario
     * @param integer $idreview
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idusuario, $idreview)
    {
        $this->findModel($idusuario, $idreview)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hacen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idusuario
     * @param integer $idreview
     * @return Hacen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idusuario, $idreview)
    {
        if (($model = Hacen::findOne(['idusuario' => $idusuario, 'idreview' => $idreview])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
