<?php

namespace app\controllers;

use Yii;
use app\models\Escribe;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EscribeController implements the CRUD actions for Escribe model.
 */
class EscribeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Escribe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Escribe::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Escribe model.
     * @param string $idautor
     * @param string $idlibro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idautor, $idlibro)
    {
        return $this->render('view', [
            'model' => $this->findModel($idautor, $idlibro),
        ]);
    }

    /**
     * Creates a new Escribe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Escribe();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idautor' => $model->idautor, 'idlibro' => $model->idlibro]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Escribe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idautor
     * @param string $idlibro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idautor, $idlibro)
    {
        $model = $this->findModel($idautor, $idlibro);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idautor' => $model->idautor, 'idlibro' => $model->idlibro]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Escribe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idautor
     * @param string $idlibro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idautor, $idlibro)
    {
        $this->findModel($idautor, $idlibro)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Escribe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idautor
     * @param string $idlibro
     * @return Escribe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idautor, $idlibro)
    {
        if (($model = Escribe::findOne(['idautor' => $idautor, 'idlibro' => $idlibro])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
