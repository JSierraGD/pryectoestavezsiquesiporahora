<?php

namespace app\controllers;

use Yii;
use app\models\Esmiembro;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EsmiembroController implements the CRUD actions for Esmiembro model.
 */
class EsmiembroController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Esmiembro models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Esmiembro::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Esmiembro model.
     * @param string $idmiembro
     * @param string $idpodcast
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idmiembro, $idpodcast)
    {
        return $this->render('view', [
            'model' => $this->findModel($idmiembro, $idpodcast),
        ]);
    }

    /**
     * Creates a new Esmiembro model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Esmiembro();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idmiembro' => $model->idmiembro, 'idpodcast' => $model->idpodcast]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Esmiembro model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idmiembro
     * @param string $idpodcast
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idmiembro, $idpodcast)
    {
        $model = $this->findModel($idmiembro, $idpodcast);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idmiembro' => $model->idmiembro, 'idpodcast' => $model->idpodcast]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Esmiembro model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idmiembro
     * @param string $idpodcast
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idmiembro, $idpodcast)
    {
        $this->findModel($idmiembro, $idpodcast)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Esmiembro model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idmiembro
     * @param string $idpodcast
     * @return Esmiembro the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idmiembro, $idpodcast)
    {
        if (($model = Esmiembro::findOne(['idmiembro' => $idmiembro, 'idpodcast' => $idpodcast])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
