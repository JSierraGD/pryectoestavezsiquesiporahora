<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreaComic */

$this->title = 'Create Crea Comic';
$this->params['breadcrumbs'][] = ['label' => 'Crea Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crea-comic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
