<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CreaComic */

$this->title = 'Update Crea Comic: ' . $model->idcreador;
$this->params['breadcrumbs'][] = ['label' => 'Crea Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcreador, 'url' => ['view', 'idcreador' => $model->idcreador, 'idcomic' => $model->idcomic]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="crea-comic-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
