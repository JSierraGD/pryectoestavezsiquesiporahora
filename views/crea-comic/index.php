<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Crea Comics';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="crea-comic-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Crea Comic', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idcreador',
            'idcomic',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
