<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Diriges';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirige-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dirige', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iddirector',
            [
                'attribute' => 'director',
                'value' => 'iddirector0.director',
            ],
            'idpelicula',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
