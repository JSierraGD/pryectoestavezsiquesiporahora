<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Es */

$this->title = 'Create Es';
$this->params['breadcrumbs'][] = ['label' => 'Es', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="es-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
