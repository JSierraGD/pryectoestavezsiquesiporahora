<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Es */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="es-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idgenero')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idpelicula')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
