<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Es */

$this->title = $model->idgenero;
$this->params['breadcrumbs'][] = ['label' => 'Es', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="es-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idgenero' => $model->idgenero, 'idpelicula' => $model->idpelicula], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idgenero' => $model->idgenero, 'idpelicula' => $model->idpelicula], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idgenero',
            'idpelicula',
        ],
    ]) ?>

</div>
