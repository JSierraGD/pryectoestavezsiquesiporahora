<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directorseries */

$this->title = 'Create Directorseries';
$this->params['breadcrumbs'][] = ['label' => 'Directorseries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directorseries-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
