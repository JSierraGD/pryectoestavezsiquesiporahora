<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Directorseries */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="directorseries-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'iddirectorserie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
