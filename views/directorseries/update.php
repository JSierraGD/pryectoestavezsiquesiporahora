<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directorseries */

$this->title = 'Update Directorseries: ' . $model->iddirectorserie;
$this->params['breadcrumbs'][] = ['label' => 'Directorseries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddirectorserie, 'url' => ['view', 'id' => $model->iddirectorserie]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directorseries-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
