<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Reviews';
$this->params['breadcrumbs'][] = $this->title;
/*
$this->widget('bootstrap.widgets.TbGridView', array(
'dataProvider'=>$dataProvider,
'columns'=>array(
     'startdate',
     'status',
      array(
            'name' => 'description',
            'value' => 'substr($data->description,0,100)."...<div class=more-data>$data->description</div><a href=javascript:void(0); id=readMore>Read More</a>',
            'type' => 'raw',
        ),
     array(
    'htmlOptions' => array('nowrap'=>'nowrap'),
    'class'=>'bootstrap.widgets.TbButtonColumn',
    'template' => '{view},{update}',
        'viewButtonUrl'=>'Yii::app()->createUrl("status/view",$params=array("id"=>$data["id"]))',
        'updateButtonUrl'=>'Yii::app()->createUrl("status/update",$params=array("id"=>$data["id"]))',
)
)))
        */
?>
<div class="reviews-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Reviews', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    
    
    
    
    
    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            'contenido',
            'puntuacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
