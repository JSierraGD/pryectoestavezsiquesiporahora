<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DirigeSeries */

$this->title = $model->idserie;
$this->params['breadcrumbs'][] = ['label' => 'Dirige Series', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="dirige-series-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idserie' => $model->idserie, 'iddirector' => $model->iddirector], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idserie' => $model->idserie, 'iddirector' => $model->iddirector], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idserie',
            'iddirector',
        ],
    ]) ?>

</div>
