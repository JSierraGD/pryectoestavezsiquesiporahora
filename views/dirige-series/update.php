<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirigeSeries */

$this->title = 'Update Dirige Series: ' . $model->idserie;
$this->params['breadcrumbs'][] = ['label' => 'Dirige Series', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idserie, 'url' => ['view', 'idserie' => $model->idserie, 'iddirector' => $model->iddirector]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dirige-series-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
