<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dirige Series';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirige-series-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Dirige Series', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idserie',
            'iddirector',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
