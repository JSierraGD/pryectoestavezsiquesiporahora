<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Podcasts */

$this->title = 'Update Podcasts: ' . $model->idpodcast;
$this->params['breadcrumbs'][] = ['label' => 'Podcasts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpodcast, 'url' => ['view', 'id' => $model->idpodcast]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="podcasts-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
