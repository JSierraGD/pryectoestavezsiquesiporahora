<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Albumes */

$this->title = 'Update Albumes: ' . $model->idalbum;
$this->params['breadcrumbs'][] = ['label' => 'Albumes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idalbum, 'url' => ['view', 'id' => $model->idalbum]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="albumes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
