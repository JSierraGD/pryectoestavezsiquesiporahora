<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Peliculas */

$this->title = 'Update Peliculas: ' . $model->idpelicula;
$this->params['breadcrumbs'][] = ['label' => 'Peliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpelicula, 'url' => ['view', 'id' => $model->idpelicula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="peliculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
