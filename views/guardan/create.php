<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Guardan */

$this->title = 'Create Guardan';
$this->params['breadcrumbs'][] = ['label' => 'Guardans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guardan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
