<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hacen */

$this->title = 'Update Hacen: ' . $model->idusuario;
$this->params['breadcrumbs'][] = ['label' => 'Hacens', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idusuario, 'url' => ['view', 'idusuario' => $model->idusuario, 'idreview' => $model->idreview]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hacen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
