<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Hacen */

$this->title = $model->idusuario;
$this->params['breadcrumbs'][] = ['label' => 'Hacens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="hacen-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idusuario' => $model->idusuario, 'idreview' => $model->idreview], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idusuario' => $model->idusuario, 'idreview' => $model->idreview], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idusuario',
            'idreview',
        ],
    ]) ?>

</div>
