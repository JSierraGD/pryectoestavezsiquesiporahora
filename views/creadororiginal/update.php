<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Creadororiginal */

$this->title = 'Update Creadororiginal: ' . $model->idcreador;
$this->params['breadcrumbs'][] = ['label' => 'Creadororiginals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcreador, 'url' => ['view', 'id' => $model->idcreador]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="creadororiginal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
