<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Creadororiginal */

$this->title = 'Create Creadororiginal';
$this->params['breadcrumbs'][] = ['label' => 'Creadororiginals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="creadororiginal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
