<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Esmiembro */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="esmiembro-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idmiembro')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'idpodcast')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
