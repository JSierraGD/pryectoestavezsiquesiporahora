<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Esmiembro */

$this->title = 'Update Esmiembro: ' . $model->idmiembro;
$this->params['breadcrumbs'][] = ['label' => 'Esmiembros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idmiembro, 'url' => ['view', 'idmiembro' => $model->idmiembro, 'idpodcast' => $model->idpodcast]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="esmiembro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
