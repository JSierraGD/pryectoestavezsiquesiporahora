<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Esmiembro */

$this->title = 'Create Esmiembro';
$this->params['breadcrumbs'][] = ['label' => 'Esmiembros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="esmiembro-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
