<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Esmiembro */

$this->title = $model->idmiembro;
$this->params['breadcrumbs'][] = ['label' => 'Esmiembros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="esmiembro-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idmiembro' => $model->idmiembro, 'idpodcast' => $model->idpodcast], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idmiembro' => $model->idmiembro, 'idpodcast' => $model->idpodcast], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idmiembro',
            'idpodcast',
        ],
    ]) ?>

</div>
