<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comics-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idcomic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_entregas')->textInput() ?>

    <?= $form->field($model, 'num_reboots')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
