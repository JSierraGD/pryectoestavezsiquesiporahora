<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = 'Create Comics';
$this->params['breadcrumbs'][] = ['label' => 'Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comics-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
