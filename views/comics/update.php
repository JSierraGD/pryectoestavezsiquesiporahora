<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Comics */

$this->title = 'Update Comics: ' . $model->idcomic;
$this->params['breadcrumbs'][] = ['label' => 'Comics', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcomic, 'url' => ['view', 'id' => $model->idcomic]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="comics-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
