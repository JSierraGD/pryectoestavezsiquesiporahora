<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirectorPeliculas */

$this->title = 'Create Director Peliculas';
$this->params['breadcrumbs'][] = ['label' => 'Director Peliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="director-peliculas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
