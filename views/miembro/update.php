<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Miembro */

$this->title = 'Update Miembro: ' . $model->idmiembro;
$this->params['breadcrumbs'][] = ['label' => 'Miembros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idmiembro, 'url' => ['view', 'id' => $model->idmiembro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="miembro-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
