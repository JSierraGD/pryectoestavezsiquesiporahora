<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Escribe */

$this->title = 'Update Escribe: ' . $model->idautor;
$this->params['breadcrumbs'][] = ['label' => 'Escribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idautor, 'url' => ['view', 'idautor' => $model->idautor, 'idlibro' => $model->idlibro]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="escribe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
