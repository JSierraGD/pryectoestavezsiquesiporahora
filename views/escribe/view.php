<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Escribe */

$this->title = $model->idautor;
$this->params['breadcrumbs'][] = ['label' => 'Escribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="escribe-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idautor' => $model->idautor, 'idlibro' => $model->idlibro], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idautor' => $model->idautor, 'idlibro' => $model->idlibro], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idautor',
            'idlibro',
        ],
    ]) ?>

</div>
