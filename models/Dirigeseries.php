<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dirigeseries".
 *
 * @property string $idserie
 * @property string $iddirector
 *
 * @property Directorseries $iddirector0
 * @property Series $idserie0
 */
class Dirigeseries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dirigeseries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idserie', 'iddirector'], 'required'],
            [['idserie', 'iddirector'], 'string', 'max' => 255],
            [['idserie', 'iddirector'], 'unique', 'targetAttribute' => ['idserie', 'iddirector']],
            [['iddirector'], 'exist', 'skipOnError' => true, 'targetClass' => Directorseries::className(), 'targetAttribute' => ['iddirector' => 'iddirectorserie']],
            [['idserie'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['idserie' => 'idserie']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idserie' => 'Idserie',
            'iddirector' => 'Iddirector',
        ];
    }

    /**
     * Gets query for [[Iddirector0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirector0()
    {
        return $this->hasOne(Directorseries::className(), ['iddirectorserie' => 'iddirector']);
    }

    /**
     * Gets query for [[Idserie0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdserie0()
    {
        return $this->hasOne(Series::className(), ['idserie' => 'idserie']);
    }
}
