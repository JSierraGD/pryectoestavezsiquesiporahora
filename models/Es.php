<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "es".
 *
 * @property string $idgenero
 * @property string $idpelicula
 *
 * @property Genero $idgenero0
 * @property Peliculas $idpelicula0
 */
class Es extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'es';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idgenero', 'idpelicula'], 'required'],
            [['idgenero', 'idpelicula'], 'string', 'max' => 255],
            [['idgenero', 'idpelicula'], 'unique', 'targetAttribute' => ['idgenero', 'idpelicula']],
            [['idgenero'], 'exist', 'skipOnError' => true, 'targetClass' => Genero::className(), 'targetAttribute' => ['idgenero' => 'idgenero']],
            [['idpelicula'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['idpelicula' => 'idpelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idgenero' => 'Idgenero',
            'idpelicula' => 'Idpelicula',
        ];
    }

    /**
     * Gets query for [[Idgenero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgenero0()
    {
        return $this->hasOne(Genero::className(), ['idgenero' => 'idgenero']);
    }

    /**
     * Gets query for [[Idpelicula0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpelicula0()
    {
        return $this->hasOne(Peliculas::className(), ['idpelicula' => 'idpelicula']);
    }
}
