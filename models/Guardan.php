<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guardan".
 *
 * @property string $idusuario
 * @property string $idmedia
 *
 * @property Usuarios $idusuario0
 * @property Media $idmedia0
 */
class Guardan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'guardan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idusuario', 'idmedia'], 'required'],
            [['idusuario', 'idmedia'], 'string', 'max' => 20],
            [['idusuario', 'idmedia'], 'unique', 'targetAttribute' => ['idusuario', 'idmedia']],
            [['idusuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['idusuario' => 'idusuario']],
            [['idmedia'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idmedia' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusuario' => 'Idusuario',
            'idmedia' => 'Idmedia',
        ];
    }

    /**
     * Gets query for [[Idusuario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuario0()
    {
        return $this->hasOne(Usuarios::className(), ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Idmedia0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmedia0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idmedia']);
    }
}
