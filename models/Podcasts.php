<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "podcasts".
 *
 * @property string $idpodcast
 * @property string|null $nombre
 * @property int|null $num_episodios
 *
 * @property Esmiembro[] $esmiembros
 * @property Miembro[] $idmiembros
 * @property Miembro $miembro
 * @property Media $idpodcast0
 */
class Podcasts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'podcasts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpodcast'], 'required'],
            [['num_episodios'], 'integer'],
            [['idpodcast', 'nombre'], 'string', 'max' => 20],
            [['idpodcast'], 'unique'],
            [['idpodcast'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idpodcast' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpodcast' => 'Idpodcast',
            'nombre' => 'Nombre',
            'num_episodios' => 'Num Episodios',
        ];
    }

    /**
     * Gets query for [[Esmiembros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEsmiembros()
    {
        return $this->hasMany(Esmiembro::className(), ['idpodcast' => 'idpodcast']);
    }

    /**
     * Gets query for [[Idmiembros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmiembros()
    {
        return $this->hasMany(Miembro::className(), ['idmiembro' => 'idmiembro'])->viaTable('esmiembro', ['idpodcast' => 'idpodcast']);
    }

    /**
     * Gets query for [[Miembro]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMiembro()
    {
        return $this->hasOne(Miembro::className(), ['idmiembro' => 'idpodcast']);
    }

    /**
     * Gets query for [[Idpodcast0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpodcast0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idpodcast']);
    }
}
