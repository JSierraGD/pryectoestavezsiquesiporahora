<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property string $idautor
 * @property string|null $autor
 *
 * @property Libros $idautor0
 * @property Escribe[] $escribes
 * @property Libros[] $idlibros
 */
class Autor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idautor'], 'required'],
            [['idautor', 'autor'], 'string', 'max' => 20],
            [['idautor'], 'unique'],
            [['idautor'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::className(), 'targetAttribute' => ['idautor' => 'idlibro']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idautor' => 'Idautor',
            'autor' => 'Autor',
        ];
    }

    /**
     * Gets query for [[Idautor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdautor0()
    {
        return $this->hasOne(Libros::className(), ['idlibro' => 'idautor']);
    }

    /**
     * Gets query for [[Escribes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEscribes()
    {
        return $this->hasMany(Escribe::className(), ['idautor' => 'idautor']);
    }

    /**
     * Gets query for [[Idlibros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdlibros()
    {
        return $this->hasMany(Libros::className(), ['idlibro' => 'idlibro'])->viaTable('escribe', ['idautor' => 'idautor']);
    }
}
