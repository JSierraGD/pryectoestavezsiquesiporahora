<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "creacomic".
 *
 * @property string $idcreador
 * @property string $idcomic
 *
 * @property Comics $idcomic0
 * @property Creadororiginal $idcreador0
 */
class Creacomic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'creacomic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcreador', 'idcomic'], 'required'],
            [['idcreador', 'idcomic'], 'string', 'max' => 255],
            [['idcreador', 'idcomic'], 'unique', 'targetAttribute' => ['idcreador', 'idcomic']],
            [['idcomic'], 'exist', 'skipOnError' => true, 'targetClass' => Comics::className(), 'targetAttribute' => ['idcomic' => 'idcomic']],
            [['idcreador'], 'exist', 'skipOnError' => true, 'targetClass' => Creadororiginal::className(), 'targetAttribute' => ['idcreador' => 'idcreador']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcreador' => 'Idcreador',
            'idcomic' => 'Idcomic',
        ];
    }

    /**
     * Gets query for [[Idcomic0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcomic0()
    {
        return $this->hasOne(Comics::className(), ['idcomic' => 'idcomic']);
    }

    /**
     * Gets query for [[Idcreador0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcreador0()
    {
        return $this->hasOne(Creadororiginal::className(), ['idcreador' => 'idcreador']);
    }
}
