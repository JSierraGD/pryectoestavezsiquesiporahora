<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "escribe".
 *
 * @property string $idautor
 * @property string $idlibro
 *
 * @property Autor $idautor0
 * @property Libros $idlibro0
 */
class Escribe extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'escribe';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idautor', 'idlibro'], 'required'],
            [['idautor', 'idlibro'], 'string', 'max' => 255],
            [['idautor', 'idlibro'], 'unique', 'targetAttribute' => ['idautor', 'idlibro']],
            [['idautor'], 'exist', 'skipOnError' => true, 'targetClass' => Autor::className(), 'targetAttribute' => ['idautor' => 'idautor']],
            [['idlibro'], 'exist', 'skipOnError' => true, 'targetClass' => Libros::className(), 'targetAttribute' => ['idlibro' => 'idlibro']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idautor' => 'Idautor',
            'idlibro' => 'Idlibro',
        ];
    }

    /**
     * Gets query for [[Idautor0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdautor0()
    {
        return $this->hasOne(Autor::className(), ['idautor' => 'idautor']);
    }

    /**
     * Gets query for [[Idlibro0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdlibro0()
    {
        return $this->hasOne(Libros::className(), ['idlibro' => 'idlibro']);
    }
}
