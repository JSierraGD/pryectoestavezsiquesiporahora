<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albumes".
 *
 * @property string $idalbum
 * @property string|null $grupo
 * @property string|null $discografica
 * @property string $nombre
 *
 * @property Media $idalbum0
 */
class Albumes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'albumes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idalbum', 'nombre'], 'required'],
            [['idalbum', 'grupo', 'discografica'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 255],
            [['idalbum'], 'unique'],
            [['idalbum'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idalbum' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idalbum' => 'Idalbum',
            'grupo' => 'Grupo',
            'discografica' => 'Discografica',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Idalbum0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdalbum0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idalbum']);
    }
}
