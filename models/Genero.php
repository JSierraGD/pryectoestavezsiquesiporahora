<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "genero".
 *
 * @property string $idgenero
 * @property string|null $peliculas_codigo
 * @property string|null $genero
 *
 * @property Es[] $es
 * @property Peliculas[] $idpeliculas
 * @property Peliculas $idgenero0
 */
class Genero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idgenero'], 'required'],
            [['idgenero', 'peliculas_codigo', 'genero'], 'string', 'max' => 20],
            [['idgenero'], 'unique'],
            [['idgenero'], 'exist', 'skipOnError' => true, 'targetClass' => Peliculas::className(), 'targetAttribute' => ['idgenero' => 'idpelicula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idgenero' => 'Idgenero',
            'peliculas_codigo' => 'Peliculas Codigo',
            'genero' => 'Genero',
        ];
    }

    /**
     * Gets query for [[Es]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEs()
    {
        return $this->hasMany(Es::className(), ['idgenero' => 'idgenero']);
    }

    /**
     * Gets query for [[Idpeliculas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdpeliculas()
    {
        return $this->hasMany(Peliculas::className(), ['idpelicula' => 'idpelicula'])->viaTable('es', ['idgenero' => 'idgenero']);
    }

    /**
     * Gets query for [[Idgenero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdgenero0()
    {
        return $this->hasOne(Peliculas::className(), ['idpelicula' => 'idgenero']);
    }
}
