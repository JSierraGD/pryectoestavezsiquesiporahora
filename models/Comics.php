<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comics".
 *
 * @property string $idcomic
 * @property int|null $num_entregas
 * @property int|null $num_reboots
 * @property string $nombre
 *
 * @property Media $idcomic0
 * @property Creacomic[] $creacomics
 * @property Creadororiginal[] $idcreadors
 * @property Creadororiginal $creadororiginal
 */
class Comics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcomic', 'nombre'], 'required'],
            [['num_entregas', 'num_reboots'], 'integer'],
            [['idcomic'], 'string', 'max' => 20],
            [['nombre'], 'string', 'max' => 255],
            [['idcomic'], 'unique'],
            [['idcomic'], 'exist', 'skipOnError' => true, 'targetClass' => Media::className(), 'targetAttribute' => ['idcomic' => 'idmedia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcomic' => 'Idcomic',
            'num_entregas' => 'Num Entregas',
            'num_reboots' => 'Num Reboots',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Idcomic0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcomic0()
    {
        return $this->hasOne(Media::className(), ['idmedia' => 'idcomic']);
    }

    /**
     * Gets query for [[Creacomics]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreacomics()
    {
        return $this->hasMany(Creacomic::className(), ['idcomic' => 'idcomic']);
    }

    /**
     * Gets query for [[Idcreadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcreadors()
    {
        return $this->hasMany(Creadororiginal::className(), ['idcreador' => 'idcreador'])->viaTable('creacomic', ['idcomic' => 'idcomic']);
    }

    /**
     * Gets query for [[Creadororiginal]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreadororiginal()
    {
        return $this->hasOne(Creadororiginal::className(), ['idcreador' => 'idcomic']);
    }
}
