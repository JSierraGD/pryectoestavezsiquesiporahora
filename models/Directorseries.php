<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directorseries".
 *
 * @property string $iddirectorserie
 * @property string|null $director
 *
 * @property Series $iddirectorserie0
 * @property Dirigeseries[] $dirigeseries
 * @property Series[] $idseries
 */
class Directorseries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directorseries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddirectorserie'], 'required'],
            [['iddirectorserie', 'director'], 'string', 'max' => 20],
            [['iddirectorserie'], 'unique'],
            [['iddirectorserie'], 'exist', 'skipOnError' => true, 'targetClass' => Series::className(), 'targetAttribute' => ['iddirectorserie' => 'idserie']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddirectorserie' => 'Iddirectorserie',
            'director' => 'Director',
        ];
    }

    /**
     * Gets query for [[Iddirectorserie0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIddirectorserie0()
    {
        return $this->hasOne(Series::className(), ['idserie' => 'iddirectorserie']);
    }

    /**
     * Gets query for [[Dirigeseries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirigeseries()
    {
        return $this->hasMany(Dirigeseries::className(), ['iddirector' => 'iddirectorserie']);
    }

    /**
     * Gets query for [[Idseries]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdseries()
    {
        return $this->hasMany(Series::className(), ['idserie' => 'idserie'])->viaTable('dirigeseries', ['iddirector' => 'iddirectorserie']);
    }
}
